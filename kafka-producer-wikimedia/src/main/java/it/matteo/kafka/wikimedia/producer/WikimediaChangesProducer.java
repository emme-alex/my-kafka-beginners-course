package it.matteo.kafka.wikimedia.producer;

import com.launchdarkly.eventsource.ConnectStrategy;
import com.launchdarkly.eventsource.EventSource;
import com.launchdarkly.eventsource.background.BackgroundEventSource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.util.concurrent.TimeUnit;

@Component
@RequiredArgsConstructor
@Slf4j
@Profile("producer-wikimedia")
public class WikimediaChangesProducer implements CommandLineRunner {

    private final KafkaTemplate<String, Object> kafkaTemplate;

    @Value("${wikimedia.recentchange.url}")
    private String wikimediaRecentChangeUrl;

    // Remember: create this topic first on Kafka
    @Value("${kafka.wikimedia.topic}")
    private String topic;

    @Override
    public void run(final String... args) throws InterruptedException {
        // create the Producer
        final KafkaProducer<String, String> kafkaProducer =
                new KafkaProducer<>(kafkaTemplate.getProducerFactory().getConfigurationProperties());

        final BackgroundEventSource.Builder builder = new BackgroundEventSource.Builder(
                new WikimediaChangeHandler(kafkaProducer, topic),
                new EventSource.Builder(
                        ConnectStrategy.http(URI.create(wikimediaRecentChangeUrl))
                ));

        final BackgroundEventSource eventSource = builder.build();
        try (eventSource) {
            eventSource.start();

            // we produce for 10 minutes and block the program until then
            TimeUnit.MINUTES.sleep(10);
        } // eventSource will be automatically closed here

        System.exit(0);
    }

}