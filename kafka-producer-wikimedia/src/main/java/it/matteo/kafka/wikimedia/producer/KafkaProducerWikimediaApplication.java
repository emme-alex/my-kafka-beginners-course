package it.matteo.kafka.wikimedia.producer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaProducerWikimediaApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KafkaProducerWikimediaApplication.class, args);
    }

}
