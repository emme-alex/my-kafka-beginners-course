package it.matteo.kafka.wikimedia.producer;

import com.launchdarkly.eventsource.MessageEvent;
import com.launchdarkly.eventsource.background.BackgroundEventHandler;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WikimediaChangeHandler implements BackgroundEventHandler {

    KafkaProducer<String, String> kafkaProducer;
    String topic;
    private final Logger log = LoggerFactory.getLogger(WikimediaChangeHandler.class.getSimpleName());

    public WikimediaChangeHandler(final KafkaProducer<String, String> kafkaProducer, final String topic) {
        this.kafkaProducer = kafkaProducer;
        this.topic = topic;
    }

    @Override
    public void onOpen() {
        log.info("BackgroundEventHandler - onOpen()");
    }

    @Override
    public void onClosed() {
        log.info("BackgroundEventHandler - onClosed()");
        kafkaProducer.close();
    }

    @Override
    public void onMessage(final String event, final MessageEvent messageEvent) {
        log.info("BackgroundEventHandler - onMessage()");
        log.info(messageEvent.getData());
        // asynchronous
        kafkaProducer.send(new ProducerRecord<>(topic, messageEvent.getData()));
    }

    @Override
    public void onComment(final String comment) {
        log.info("BackgroundEventHandler - onComment()");
    }

    @Override
    public void onError(final Throwable t) {
        log.info("BackgroundEventHandler - onError()");
        log.error("Error in Stream Reading", t);
    }

}
