package it.matteo.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.CooperativeStickyAssignor;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
@RequiredArgsConstructor
@Slf4j
@Profile("consumer")
public class ConsumerDemo implements CommandLineRunner {

    private final ConsumerFactory<String, String> consumerFactory;

    private final static String TOPIC = "logs";

    @Override
    public void run(final String... args) {
        consuming(consumerFactory.getConfigurationProperties());
        consumingWithShutdown(consumerFactory.getConfigurationProperties());
        consumingCooperative(consumerFactory.getConfigurationProperties());

        System.exit(0);
    }

    public void consuming(final Map<String, Object> configurationProperties) {
        // create a consumer
        final KafkaConsumer<String, String> kafkaConsumer =
                new KafkaConsumer<>(configurationProperties);

        try (kafkaConsumer) {
            // subscribe to a topic
            kafkaConsumer.subscribe(List.of(TOPIC));

            while (true) {
                log.info("Polling...");

                final ConsumerRecords<String, String> records =
                        kafkaConsumer.poll(Duration.ofSeconds(1));
                records.forEach(record -> {
                    log.info("Key: {}, Value: {}", record.key(), record.value());
                    log.info("Partition: {}, Offset: {}", record.partition(), record.offset());
                });
            }
        }
    }

    public void consumingWithShutdown(final Map<String, Object> configurationProperties) {
        // create a consumer
        final KafkaConsumer<String, String> kafkaConsumer =
                new KafkaConsumer<>(configurationProperties);

        // subscribe to a topic
        kafkaConsumer.subscribe(List.of(TOPIC));

        // get a reference to the main thread
        final Thread mainThread = Thread.currentThread();

        // adding the shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Detected a shutdown, let's exit by calling consumer.wakeup()...");
            kafkaConsumer.wakeup();

            // join the main thread to allow the execution of the code in the main thread
            try {
                mainThread.join();
            } catch (final InterruptedException e) {
            }
        }));

        try {
            // subscribe to a topic
            kafkaConsumer.subscribe(List.of(TOPIC));
            // poll for data
            while (true) {
                final ConsumerRecords<String, String> records =
                        kafkaConsumer.poll(Duration.ofMillis(1000));

                for (final ConsumerRecord<String, String> record : records) {
                    log.info("Key: " + record.key() + ", Value: " + record.value());
                    log.info("Partition: " + record.partition() + ", Offset: " + record.offset());
                }
            }
        } catch (final WakeupException e) {
            log.info("Consumer is starting to shut down");
        } catch (final Exception e) {
            log.error("Unexpected exception in the consumer", e);
        } finally {
            kafkaConsumer.close(); // close the consumer, this will also commit offsets
            log.info("The consumer is now gracefully shut down");
        }
    }

    private void consumingCooperative(final Map<String, Object> configurationProperties) {
        final HashMap<String, Object> newProperties = new HashMap<>(configurationProperties);
        newProperties
                .put("partition.assignment.strategy", CooperativeStickyAssignor.class.getName());
        consumingWithShutdown(newProperties);
    }

}