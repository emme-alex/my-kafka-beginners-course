package it.matteo.kafka;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaBasicsApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KafkaBasicsApplication.class, args);
    }

}
