package it.matteo.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.atomic.AtomicInteger;

@Component
@RequiredArgsConstructor
@Slf4j
@Profile("producer")
public class ProducerDemo implements CommandLineRunner {

    private final KafkaTemplate<String, String> kafkaTemplate;

    private final static String TOPIC = "logs";

    @Override
    public void run(final String... args) {
        producingNoKeys();
        producingWithKeys();

        System.exit(0);
    }

    private void producingWithKeys() {
        sendSyncCallbackKeysKafkaProducer(TOPIC,
                List.of("msg_key1", "msg_key2", "msg_key3", "msg_key4", "msg_key5", "msg_key6"));
    }

    private void producingNoKeys() {
        // - For each run the partition where it's been writing to is DIFFERENT
        // - When messages are sent in time close to each other, the partition where they are written to is the SAME,
        //   that's because Kafka Java SDK optimizes and does not use "Round Robin", it uses "Sticky Partitioner" (UniformStickyPartitioner)

        sendSyncKafkaProducer(TOPIC, List.of("Hello Kafka!", "Hello, World! sync"));
        sendSyncKafkaProducer("twitter_tweets", List.of("Hello Kafka!", "Test sync"));

        sendSyncCallbackKafkaProducer(TOPIC,
                List.of("Callback message", "Test callback sync", "Another callback message", "Boh callback"));

        sendSyncCallbackKafkaTemplate(TOPIC, List.of("Hello Kafka template", "Kafka template"));
    }

    private void sendSyncKafkaProducer(final String topic, final List<String> messages) {
        // create the Producer
        final KafkaProducer<String, String> kafkaProducer =
                new KafkaProducer<>(kafkaTemplate.getProducerFactory().getConfigurationProperties());

        messages.forEach(message -> {
            // create a Producer Record
            final ProducerRecord<String, String> producerRecord =
                    new ProducerRecord<>(topic, message);

            // send data
            kafkaProducer.send(producerRecord);
        });

        // tell the producer to send all data and block until done -- synchronous
        kafkaProducer.flush();

        //  close the producer
        kafkaProducer.close();
    }

    private void sendSyncCallbackKafkaProducer(final String topic, final List<String> messages) {
        final KafkaProducer<String, String> kafkaProducer =
                new KafkaProducer<>(kafkaTemplate.getProducerFactory().getConfigurationProperties());

        messages.forEach(message -> {
            final ProducerRecord<String, String> producerRecord =
                    new ProducerRecord<>(topic, message);

            kafkaProducer.send(producerRecord, (metadata, ex) -> {
                // executes every time a record successfully sent or an exception is thrown
                if (ex == null) {
                    // the record was successfully sent
                    log.info(
                            "Received new metadata \n Topic: {}\n Message: {}\n Partition: {}\n Offset: {}\n Timestamp: {}\n",
                            metadata.topic(), message, metadata.partition(), metadata.offset(), metadata.timestamp());
                } else {
                    log.error("Error while producing", ex);
                }
            });
        });

        // tell the producer to send all data and block until done -- synchronous
        kafkaProducer.flush();

        //  close the producer
        kafkaProducer.close();
    }

    private void sendSyncCallbackKafkaTemplate(final String topic, final List<String> messages) {
        messages.forEach(message -> {
            // Send the message and get a CompletableFuture for the result
            final CompletableFuture<SendResult<String, String>> future =
                    kafkaTemplate.send(topic, message).toCompletableFuture();
            // Add a callback to print the producer metadata when the message is successfully sent
            future.thenAccept(result -> {
                // Get the producer record metadata from the result
                final RecordMetadata metadata = result.getRecordMetadata();
                // Print the topic, partition, offset and timestamp of the message
                log.info(
                        "Received new metadata \n Topic: {}\n Message: {}\n Partition: {}\n Offset: {}\n Timestamp: {}\n",
                        metadata.topic(), message, metadata.partition(), metadata.offset(), metadata.timestamp());
            }).exceptionally(ex -> {
                // Handle the failure case
                log.error("Error to send message {}", message, ex);
                return null;
            });
        });
    }

    private void sendSyncCallbackKeysKafkaProducer(final String topic, final List<String> messages) {
        final HashMap<String, Object> newProperties = new HashMap<>(kafkaTemplate.getProducerFactory().getConfigurationProperties());
        newProperties
                .put("batch.size", "400");
        final KafkaProducer<String, String> kafkaProducer =
                new KafkaProducer<>(newProperties);

        for (int i = 0; i < 2; i++) {
            final AtomicInteger index = new AtomicInteger(0);
            messages.forEach(message -> {
                final String key = "id_" + index.getAndIncrement();
                final String value = "hello world" + index.getAndIncrement();

                final ProducerRecord<String, String> producerRecord =
                        new ProducerRecord<>(topic, key, value);

                kafkaProducer.send(producerRecord, (metadata, ex) -> {
                    if (ex == null) {
                        log.info("Key: " + key + " | Partition: " + metadata.partition());
                    } else {
                        log.error("Error while producing", ex);
                    }
                });
            });

            try {
                Thread.sleep(500);
            } catch (final InterruptedException e) {
                throw new RuntimeException(e);
            }
        }

        // tell the producer to send all data and block until done -- synchronous
        kafkaProducer.flush();

        //  close the producer
        kafkaProducer.close();
    }

}