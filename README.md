# Kafka for Beginners

Personal repo produced while learning following 
[Kafka for Beginners course](https://www.udemy.com/certificate/UC-93c63a58-8b9f-4253-a5af-6e2813780b22/)

## Content

- Basics of Kafka Java Programming
- Wikimedia Producer
- OpenSearch Consumer
- Kafka Streams Sample Application

## Local run

```shell
# kafka-basics
./mvnw -Pproducer-dev
./mvnw -Pconsumer-dev

# kafka-producer-wikimedia
./mvnw -Pproducer-wikimedia-dev

# kafka-streams-wikimedia
./mvnw -Pstreams-wikimedia-dev

# kafka-consumer-opensearch
./mvnw -Popensearch-dev
```