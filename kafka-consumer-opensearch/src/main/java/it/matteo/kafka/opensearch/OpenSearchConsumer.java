package it.matteo.kafka.opensearch;

import com.google.gson.JsonParser;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.errors.WakeupException;
import org.opensearch.action.bulk.BulkRequest;
import org.opensearch.action.bulk.BulkResponse;
import org.opensearch.action.index.IndexRequest;
import org.opensearch.client.RequestOptions;
import org.opensearch.client.RestHighLevelClient;
import org.opensearch.client.indices.CreateIndexRequest;
import org.opensearch.client.indices.GetIndexRequest;
import org.opensearch.common.xcontent.XContentType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Profile;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.util.Collections;

@Component
@RequiredArgsConstructor
@Slf4j
@Profile("opensearch-consumer")
public class OpenSearchConsumer implements CommandLineRunner {

    private final ConsumerFactory<String, String> consumerFactory;

    @Value("${opensearch.url}")
    private String opensearchUrl;

    @Value("${kafka.wikimedia.topic}")
    private String topic;

    private static final String WIKIMEDIA_OPENSEARCH_INDEX = "wikimedia";

    /**
     * Reads data from Kafka topic (using KafkaConsumer) and writes to OpenSearch (specific index)!
     *
     * @param args
     * @throws IOException
     */
    @Override
    public void run(final String... args) throws IOException {
        final RestHighLevelClient openSearchClient = OpenSearchUtils.createOpenSearchClient(opensearchUrl);

        // create our Kafka Client
        final KafkaConsumer<String, String> consumer = createKafkaConsumer();

        // get a reference to the main thread
        final Thread mainThread = Thread.currentThread();

        // adding the shutdown hook
        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            log.info("Detected a shutdown, let's exit by calling consumer.wakeup()...");
            consumer.wakeup();

            // join the main thread to allow the execution of the code in the main thread
            try {
                mainThread.join();
            } catch (final InterruptedException e) {
            }
        }));

        // we need to create the index on OpenSearch if it doesn't exist already
        try (openSearchClient; consumer) {
            final boolean indexExists = openSearchClient.indices().exists(
                    new GetIndexRequest(WIKIMEDIA_OPENSEARCH_INDEX), RequestOptions.DEFAULT);

            if (!indexExists) {
                final CreateIndexRequest createIndexRequest = new CreateIndexRequest(WIKIMEDIA_OPENSEARCH_INDEX);
                openSearchClient.indices().create(createIndexRequest, RequestOptions.DEFAULT);
                log.info(String.format("The index %s has been created!", WIKIMEDIA_OPENSEARCH_INDEX));
            } else {
                log.info(String.format("The index %s already exits!", WIKIMEDIA_OPENSEARCH_INDEX));
            }

            // we subscribe the consumer
            consumer.subscribe(Collections.singleton(topic));

            while (true) {
                final ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(3000));

                final int recordCount = records.count();
                log.info("Received " + recordCount + " record(s)");

                sendRecordsToOpenSearch(records, consumer, openSearchClient);
            }
        } catch (final WakeupException e) {
            log.info("Consumer is starting to shut down");
        } catch (final Exception e) {
            log.error("Unexpected exception in the consumer", e);
        } finally {
            consumer.close(); // close the consumer, this will also commit offsets
            openSearchClient.close();
            log.info("The consumer is now gracefully shut down");
        }

        System.exit(0);
    }

    private void sendRecordsToOpenSearch(
            final ConsumerRecords<String, String> records,
            final KafkaConsumer<String, String> consumer,
            final RestHighLevelClient openSearchClient) throws IOException {

        final BulkRequest bulkRequest = new BulkRequest();

        records.forEach(record -> {
            // send the record into OpenSearch

            //
            // NB: IDEMPOTENT CONSUMER (avoid duplicates)
            //

            //
            // Strategy 1 (to avoid sending duplicates to OpenSearch by extracting the ID == IDEMPOTENT):
            // define an ID using Kafka Record coordinates
            //
//                    String id = record.topic() + "_" + record.partition() + "_" + record.offset();

            try {
                //
                // Strategy 2 (to avoid sending duplicates to OpenSearch by extracting the ID == IDEMPOTENT):
                //
                // we extract the ID from the JSON value
                final String id = extractId(record.value());
//                log.info("Extracted record id: {}", id);

                final IndexRequest indexRequest = new IndexRequest(WIKIMEDIA_OPENSEARCH_INDEX)
                        .source(record.value(), XContentType.JSON)
                        .id(id);

                bulkRequest.add(indexRequest);

//                IndexResponse response = openSearchClient.index(indexRequest, RequestOptions.DEFAULT);
//                log.info(response.getId());
            } catch (final Exception e) {
                log.error("while sending the record into OpenSearch", e);
            }
        });

        // send to OpenSearch ONLY IF we have records
        if (bulkRequest.numberOfActions() > 0) {
            final BulkResponse bulkResponse = openSearchClient.bulk(bulkRequest, RequestOptions.DEFAULT);
            log.info("Inserted " + bulkResponse.getItems().length + " record(s).");

            try {
                Thread.sleep(1000);
            } catch (final InterruptedException e) {
            }

            // commit offsets after the batch is consumed
            //
            // NB:
            // the offsets are manually committed only AFTER we've successfully processed the entire batch,
            // so it's an "at-least-once" strategy.
            consumer.commitSync();
            log.info("Offsets have been committed!");
        }
    }

    // @link https://www.stackchief.com/blog/Spring%20Boot%20Kafka%20Consumer
//    @KafkaListener(
//            topics = "${kafka.wikimedia.topic}",
//            concurrency = "2",
//            groupId = "${spring.kafka.consumer.group-id}"
//    )
//    public void processMessage(final String content) {
//        log.info("Message received: {}", content);
//    }

    private KafkaConsumer<String, String> createKafkaConsumer() {
        return new KafkaConsumer<>(consumerFactory.getConfigurationProperties());
    }

    private String extractId(final String json) {
        // gson library
        return JsonParser.parseString(json)
                .getAsJsonObject()
                .get("meta")
                .getAsJsonObject()
                .get("id")
                .getAsString();
    }

}