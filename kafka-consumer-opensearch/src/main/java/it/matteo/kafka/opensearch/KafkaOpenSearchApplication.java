package it.matteo.kafka.opensearch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaOpenSearchApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KafkaOpenSearchApplication.class, args);
    }

}
