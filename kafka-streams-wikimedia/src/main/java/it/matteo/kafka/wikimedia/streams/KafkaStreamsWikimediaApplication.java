package it.matteo.kafka.wikimedia.streams;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaStreamsWikimediaApplication {

    public static void main(final String[] args) {
        SpringApplication.run(KafkaStreamsWikimediaApplication.class, args);
    }

}
