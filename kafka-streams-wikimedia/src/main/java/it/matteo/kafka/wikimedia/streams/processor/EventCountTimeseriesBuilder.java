package it.matteo.kafka.wikimedia.streams.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.WindowedSerdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.util.Map;

@Component
public class EventCountTimeseriesBuilder {

    @Value("${kafka.wikimedia.topic.stats-timeseries}")
    private String timeseriesTopic;

    @Value("${kafka.wikimedia.store.event-count-store}")
    private String timeseriesStore;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    public void setup(final KStream<String, String> inputStream) {
        final TimeWindows timeWindows = TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(10));
        inputStream
                .selectKey((key, value) -> "key-to-group")
                .groupByKey()
                .windowedBy(timeWindows)
                .count(Materialized.as(timeseriesStore))
                .toStream()
                .mapValues((readOnlyKey, value) -> {
                    final Map<String, Object> kvMap = Map.of(
                            "start_time", readOnlyKey.window().startTime().toString(),
                            "end_time", readOnlyKey.window().endTime().toString(),
                            "window_size", timeWindows.size(),
                            "event_count", value
                    );
                    try {
                        return OBJECT_MAPPER.writeValueAsString(kvMap);
                    } catch (final JsonProcessingException e) {
                        return null;
                    }
                })
                .to(timeseriesTopic, Produced.with(
                        WindowedSerdes.timeWindowedSerdeFrom(String.class, timeWindows.size()),
                        Serdes.String()
                ));
    }
}
