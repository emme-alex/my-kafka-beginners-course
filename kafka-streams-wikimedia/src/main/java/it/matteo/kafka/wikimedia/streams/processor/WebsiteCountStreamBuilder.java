package it.matteo.kafka.wikimedia.streams.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.apache.kafka.streams.kstream.Produced;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.apache.kafka.streams.kstream.WindowedSerdes;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.time.Duration;
import java.util.Map;

@Component
public class WebsiteCountStreamBuilder {

    @Value("${kafka.wikimedia.store.website-count}")
    private String websiteCountStore;

    @Value("${kafka.wikimedia.topic.stats-website}")
    private String websiteCountTopic;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();


    public void setup(final KStream<String, String> inputStream) {
        final TimeWindows timeWindows = TimeWindows.ofSizeWithNoGrace(Duration.ofMinutes(1L));
        inputStream
                .selectKey((k, changeJson) -> {
                    try {
                        final JsonNode jsonNode = OBJECT_MAPPER.readTree(changeJson);
                        return jsonNode.get("server_name").asText();
                    } catch (final IOException e) {
                        return "parse-error";
                    }
                })
                .groupByKey()
                .windowedBy(timeWindows)
                .count(Materialized.as(websiteCountStore))
                .toStream()
                .mapValues((key, value) -> {
                    final Map<String, Object> kvMap = Map.of(
                            "website", key.key(),
                            "count", value
                    );
                    try {
                        return OBJECT_MAPPER.writeValueAsString(kvMap);
                    } catch (final JsonProcessingException e) {
                        return null;
                    }
                })
                .to(websiteCountTopic, Produced.with(
                        WindowedSerdes.timeWindowedSerdeFrom(String.class, timeWindows.size()),
                        Serdes.String()
                ));
    }
}
