package it.matteo.kafka.wikimedia.streams.processor;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.Materialized;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.Map;

@Component
public class BotCountStreamBuilder {

    @Value("${kafka.streams.bot.count-store}")
    private String botCountStore;

    @Value("${kafka.streams.bot.wikimedia-stats}")
    private String botCountTopic;

    private static final ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public void setup(final KStream<String, String> inputStream) {
        inputStream
                .mapValues(changeJson -> {
                    try {
                        final JsonNode jsonNode = OBJECT_MAPPER.readTree(changeJson);
                        if (jsonNode.get("bot").asBoolean()) {
                            return "bot";
                        }
                        return "non-bot";
                    } catch (final IOException e) {
                        return "parse-error";
                    }
                })
                .groupBy((key, botOrNot) -> botOrNot)
                .count(Materialized.as(botCountStore))
                .toStream()
                .mapValues((key, value) -> {
                    final Map<String, Long> kvMap = Map.of(String.valueOf(key), value);
                    try {
                        return OBJECT_MAPPER.writeValueAsString(kvMap);
                    } catch (final JsonProcessingException e) {
                        return null;
                    }
                })
                .to(botCountTopic);
    }
}
