package it.matteo.kafka.wikimedia.streams.processor;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.Topology;
import org.apache.kafka.streams.kstream.KStream;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.autoconfigure.kafka.KafkaProperties;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.Properties;


@Component
@RequiredArgsConstructor
@Slf4j
@Profile("streams-wikimedia")
public class WikimediaStreamsApp implements CommandLineRunner {

    private final KafkaProperties kafkaProperties;

    // Remember: create this topic first on Kafka
    @Value("${kafka.wikimedia.topic}")
    private String topic;

    private final BotCountStreamBuilder botCountStreamBuilder;
    private final WebsiteCountStreamBuilder websiteCountStreamBuilder;
    private final EventCountTimeseriesBuilder eventCountTimeseriesBuilder;

    @Override
    public void run(final String... args) {
        final Properties properties = new Properties();
        properties.putAll(kafkaProperties.getProperties());
        properties.putAll(kafkaProperties.getStreams().getProperties());
        properties.put(StreamsConfig.APPLICATION_ID_CONFIG, kafkaProperties.getStreams().getApplicationId());
        properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        log.info("Properties: {}", properties);

        final Topology appTopology = getTopology();
        log.info("Topology: {}", appTopology.describe());

        // 1. start this Wikipedia Streams consumer
        // 2. start Wikimedia Producer to produce some data
        final KafkaStreams streams = new KafkaStreams(appTopology, properties); // do not put this in a "try" otherwise it stops on start
        streams.start();
        Runtime.getRuntime().addShutdownHook(new Thread(streams::close));
    }

    private Topology getTopology() {
        final StreamsBuilder builder = new StreamsBuilder();
        final KStream<String, String> changeJsonStream = builder.stream(topic);

        botCountStreamBuilder.setup(changeJsonStream);
        websiteCountStreamBuilder.setup(changeJsonStream);
        eventCountTimeseriesBuilder.setup(changeJsonStream);

        return builder.build();
    }

}
